﻿using FluentMigrator;

namespace Cosmic_objects
{
    [Migration(202104280909, "DKS-96")]
    public class Migration_202104280909 : Migration
    {
        public override void Up()
        {
            //// ORGANISATIONS
            //Create.Table("planet")
            //    .WithColumn("planetid").AsInt64().PrimaryKey().Identity()
            //    .WithColumn("name").AsString(256)
            //    .WithColumn("radius").AsString(256).Nullable()
            //    .WithColumn("size").AsDouble().Nullable()
            //    .WithColumn("speed").AsDouble().Nullable();

            //Create.Table("star")
            //    .WithColumn("starid").AsInt64().PrimaryKey().Identity()
            //    .WithColumn("name").AsString(256)
            //    .WithColumn("radius").AsString(256).Nullable()
            //    .WithColumn("size").AsDouble().Nullable()
            //    .WithColumn("distance_to_the_star").AsInt32().Nullable();

            //Create.Table("asteroid")
            //    .WithColumn("asteroidid").AsInt64().PrimaryKey().Identity()
            //    .WithColumn("name").AsString(256)
            //    .WithColumn("radius").AsString(256).Nullable()
            //    .WithColumn("size").AsDouble().Nullable()
            //    .WithColumn("speed").AsDouble().Nullable();

            //Create.Table("black_hole")
            //    .WithColumn("black_holeid").AsInt64().PrimaryKey().Identity()
            //    .WithColumn("name").AsString(256)
            //    .WithColumn("radius").AsString(256).Nullable()
            //    .WithColumn("size").AsDouble().Nullable()
            //    .WithColumn("opening_year").AsInt32().NotNullable();

            Create.Table("phone")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("company").AsString(256)
                .WithColumn("price").AsInt64().Nullable();

            Create.Table("order")
                .WithColumn("orderId").AsInt64().PrimaryKey().Identity()
                .WithColumn("user").AsString(256).Nullable()
                .WithColumn("address").AsString(256).Nullable()
                .WithColumn("contactPhone").AsString(256)
                .WithColumn("phoneId").AsInt64();
                
        }       

        public override void Down()
        {

        }
    }
}
