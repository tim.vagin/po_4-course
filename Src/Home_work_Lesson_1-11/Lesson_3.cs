﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Lesson_3
    {
        public int MyProperty { get; set; }

        /*Датчиком случайных чисел сгенерируйте четыре натуральных числа в интервале
        от 1 до 900. Определите, сколько цифр содержит сумма полученных чисел.
        Нельзя использовать строки, списки, массивы.*/
            //Тест.Полученные числа: 567; 41; 138; 862.
         //Результат.Сумма 1608 содержит 4 цифры.

       static public void Execute()
        {
            Console.WriteLine("Датчиком случайных чисел сгенерируйте четыре натуральных числа в интервале");
                 Console.WriteLine("от 1 до 900. Определите, сколько цифр содержит сумма полученных чисел.");
            Console.WriteLine("Нельзя использовать строки, списки, массивы");

            int Sum = 0;
            Random rnd = new();

            for (int i = 0; i < 4; i++)
            {
                int Value = rnd.Next(1, 900); Console.WriteLine(Value);
                Sum += Value;

            }
            if (Sum < 10)
                Console.WriteLine(" 1 число"); // Добавляем изменения
            else if (10 < Sum && Sum < 99)
                Console.WriteLine(" 2 числа");  
            else if (100 < Sum && Sum < 999)
                Console.WriteLine(" 3 числа");
            else if (1000 < Sum && Sum < 9999)
                Console.WriteLine(" 4 числа");

            Console.WriteLine(Sum);
        }
    }
}
