﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Part2_1
    {
      //  public int MyProperty { get; set; }

        /*. Считать строки из файла и отобразить строки, которые содержат искомое слово 
               W. При реализации пользоваться классами, разделив их по назначению.*/

         public void Execute()
        {   // Ссылка на файл
            string path = @"C:\Users\97253\OneDrive\Рабочий стол\content.txt";
            // Текст для проверки
         
            string fileText = File.ReadAllText(path);

            string[] buf = fileText.Split(new char[] { '\n' });

            foreach ( string line in buf)
            {
                if (line.IndexOf('w') != -1)
                    Console.WriteLine(line);
            }

            
        }
    }
}
