﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Lesson_5
    {
        public int MyProperty { get; set; }

        /*Имеются две переменные одинакового типа: целые или вещественные.
        Обменяйте их значения: а) используя дополнительную переменную; б) не вводя
         дополнительную переменную.*/

        public void Execute()
        {
            Console.WriteLine("Имеются две переменные одинакового типа: целые или вещественные.Обменяйте их значения: ");
            Console.WriteLine("а) используя дополнительную переменную; б) не вводя дополнительную переменную.");
            int a = 27;
            int b = 35;
            int buf;
            // a)
            buf = a;
            a = b;
            b = buf;
            Console.WriteLine(a);
            Console.WriteLine(b);

            // b)

            a = a + b;
            b = b - a;
            b = -b;
            a = a - b;

            Console.WriteLine(a);
            Console.WriteLine(b);

        }
    }
}
