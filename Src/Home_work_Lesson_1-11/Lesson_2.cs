﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Lesson_2
    {
        public int MyProperty { get; set; }

        //Напечатайте на экране все двузначные числа, кратные данному числу k
        public void Execute()
        {
            Console.WriteLine("Напечатайте на экране все двузначные числа, кратные данному числу k");
            int k = Convert.ToInt32(Console.ReadLine());
            for (int i = 10; i <= 99; i++)
                {
                if (i % k == 0)
                    Console.WriteLine(i);
            }

        }
    }
}
