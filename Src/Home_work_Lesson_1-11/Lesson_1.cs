﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Lesson_1
    {
        public int MyProperty { get; set; }

        /*Напечатайте на экране монитора числа, принадлежащие отрезку[1; 99] и
        кратные числу 3.*/
        
        public void Execute()
        {
            Console.WriteLine("Напечатайте на экране монитора числа, принадлежащие отрезку[1; 99] и кратные числу 3");
            for (int i = 1; i <= 99; i++)
            {
                if (i % 3 == 0)
                    Console.WriteLine(i);
            }
        }
    }
}      
    

