﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Lesson_8
    {
        public int MyProperty { get; set; }

        /*Дано натуральное число N.Вычислите сумму его цифр.При решении
           использовать рекурсию. Нельзя использовать строки, списки, массивы.*/

        public void Execute()
        {
            Console.WriteLine("Дано натуральное число N.Вычислите сумму его цифр.При решении использовать рекурсию.Нельзя использовать строки, списки, массивы.");
            int Sum(int x, int a, int b)
            {
                a = x % 10;
                if (a == 0) return b;
                else
                {
                    b += a;
                    return Sum(x / 10, a, b);
                }
            }


            int N = Convert.ToInt32(Console.ReadLine());
            int a = 0; int b = 0;
            int S = Sum(N, a, b);

            Console.WriteLine(S);
        }
    }
}