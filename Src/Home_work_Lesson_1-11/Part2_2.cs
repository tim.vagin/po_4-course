﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Part2_2
    {

        //  public int MyProperty { get; set; }

        /*. Реализовать стек строк, в который можно добавить и удалять элементы. При добавлении и удалении элементов должно запускаться событие*/
        public delegate void EventHandler(string message);
        public event EventHandler? Notify;
        Stack<string> stack = new();
        public void Push(string str)
        {

        // Добавляем строки в стек
            
                string? inf = null;
                //var str = Console.ReadLine();
                stack.Push(str);
                
                    foreach (var obj in stack)
                        inf += obj + "\n";
                    inf = inf[0..^1];
                    Notify?.Invoke(inf);
           
        }
        //Удаляем строку из стека и вызываем событие
        public void Pop()
        {
            string? inf = null;
            if (stack.Count > 0)
            {
                stack.Pop();


                foreach (var obj in stack)
                    inf += obj + "\n";

                if (stack.Count > 1)
                    inf = inf[0..^1];
            }
            Notify?.Invoke(inf);
        }
      
    }
}