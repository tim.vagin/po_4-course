﻿
using ConsoleApp3;
using System;
using System.Threading.Tasks;

namespace study
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите номер задания:");
            int Numb = Convert.ToInt32(Console.ReadLine());
            if (Numb == 1)
            {
                var Les_1 = new Lesson_1();
                Les_1.Execute();
            }
            if (Numb == 2)
            {
                var Les_2 = new Lesson_2();
                Les_2.Execute();
            }
            if (Numb == 3)
            {
                var Les_3 = new Lesson_3();
                Les_3.Execute();
            }
            if (Numb == 4)
            {
                var Les_4 = new Lesson_4();
                Les_4.Execute();
            }
            if (Numb == 5)
            {
                var Les_5 = new Lesson_5();
                Les_5.Execute();
            }
            if (Numb == 6)
            {
                var Les_6 = new Lesson_6();
                Les_6.Execute();
            }
            if (Numb == 7)
            {
                var Les_7 = new Lesson_7();
                Les_7.Execute();
            }
            if (Numb == 8)
            {
                var Les_8 = new Lesson_8();
                Les_8.Execute();
            }
            if (Numb == 9)
            {
                var Les_9 = new Lesson_9();
                Les_9.Execute();
            }
            if (Numb == 10)
            {
                var P2 = new Part2_1();
                P2.Execute();
            }
            if (Numb == 11)
            {

                Console.WriteLine("Реализовать стек строк, в который можно добавить и удалять элементы.");
                Console.WriteLine("При добавлении и удалении элементов должно запускаться событие");
                Console.WriteLine("Для удаления нажмите Enter, для выхода нажмите 9");

                var P2_2 = new Part2_2();
                P2_2.Notify += P2_2_Notify;
                string str = null;
                while (str != "9")
                {
                    str = Console.ReadLine();
                    if (str != "" && str != "9")
                        P2_2.Push(str);

                    if (str == "")
                        P2_2.Pop();

                }

            }
            

        }
            private static void P2_2_Notify(string message)
            {
                // throw new NotImplementedException();
                if (message != null)
                    Console.WriteLine($"Стек содержит следующие элементы:\n{message}");
                else
                    Console.WriteLine($"Стек содержит следующие элементы:\nСтек пустой");
            }

        }
    }
