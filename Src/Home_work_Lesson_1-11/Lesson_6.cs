﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Lesson_6
    {
        public int MyProperty { get; set; }

        /*Напишите программу нахождения всех трехзначных чисел, сумма цифр которых
           равна данному числу k. Нельзя использовать строки, списки, массивы.
           Тест. 1) k=26;2) k=25.
           Результат. 1) 899; 989; 998. 2) 799; 889; 898; 979; 988; 997.*/

        public void Execute()
        {
            Console.WriteLine("Напишите программу нахождения всех трехзначных чисел, сумма цифр которых равна данному числу k.");
            Console.WriteLine("Нельзя использовать строки, списки, массивы.Тест. 1) k = 26; 2) k = 25");
            Console.WriteLine("Результат. 1) 899; 989; 998. 2) 799; 889; 898; 979; 988; 997.");
            int k = Convert.ToInt32(Console.ReadLine());
            int a, b, c;


            for (int i = 100; i < 999; i++)
            {
                int d = i;
                a = d / 100;
                d = d % 100;
                b = d / 10;
                c = d % 10;
               if ( (a + b + c) == k )
                    Console.WriteLine(i);
            }
        }
    }
}