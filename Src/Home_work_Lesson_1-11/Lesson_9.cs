﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Lesson_9
    {
        public int MyProperty { get; set; }

        /*Дано натуральное число N. Вычислить количество единиц. При решении
            использовать рекурсию. Нельзя использовать строки, списки, массивы..*/

        public void Execute()
        {
            Console.WriteLine("Дано натуральное число N. Вычислить количество единиц. При решении");
            Console.WriteLine(" использовать рекурсию. Нельзя использовать строки, списки, массивы..");
            int Count_1(int x, int i, int a)
            {
                a = x % 10;
                if (a == 1) i++;
                if (a == 0) return i;
                else
                    return Count_1(x / 10, i, a);
            }


            int N = Convert.ToInt32(Console.ReadLine());
            int i = 0; int a = 0;
            int C = Count_1(N, i, a);
            Console.WriteLine(C);
        }   }
}