﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Lesson_7
    {
        public int MyProperty { get; set; }

        /*Напечатать на экране последовательность Фиббоначи из k элементов. При
         решении использовать рекурсию. Нельзя использовать строки, списки,
          массивы.*/

        public void Execute()
        {

            Console.WriteLine("Напечатать на экране последовательность Фиббоначи из k элементов. При решении использовать рекурсию.");
            Console.WriteLine("Нельзя использовать строки, списки, массивы.");

            int Fibonachi(int n)
            {
                if (n == 0 || n == 1) return n;

                return Fibonachi(n - 1) + Fibonachi(n - 2);
            }

            int k = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= k; i++)
            {
                int fib4 = Fibonachi(i);
                Console.Write(fib4);
                if (i != k)
                Console.Write(", ");
            }
        }
    }
}