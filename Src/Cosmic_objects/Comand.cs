﻿namespace Cosmic_objects
{
    public class Comand
    {
        QueriesDB _data;
        Addition _add;
        Deletion _del;
        Data _values;
        XmlMethods _xml;
        UserMonitor _user;
        public Comand(QueriesDB database, Addition add, Deletion del, Data data, XmlMethods methods, UserMonitor User)
        {
            _data = database;
            _add = add;
            _del = del;
            _values = data;
            _xml = methods;
            _user = User;
        }
        // Обработчик пользовательских команд
        public void Comand_Handler(UserMonitor.Command key, string str, List<object> cosmic_Objects)
        {
            switch (key)
            {
                case UserMonitor.Command.Add_planet:                                              // Добавлям Планету
                    CreatePlanet(str, cosmic_Objects);
                    break;

                case UserMonitor.Command.Add_star:                                                // Добавляем Звезду
                    CreateStar(str, cosmic_Objects);
                    break;

                case UserMonitor.Command.Add_asteroid:                                            // Добавляем Астероид
                    CreateAsteroid(str, cosmic_Objects);
                    break;

                case UserMonitor.Command.Add_black_hole:                                          // Добавляем Черную Дыру
                    CreateBlack_hole(str, cosmic_Objects);
                    break;

                case UserMonitor.Command.del:                                                     // Удаляем объект
                    Delete(str, cosmic_Objects);
                    break;

                case UserMonitor.Command.print:                                                   // Печатаем все
                    cosmic_Objects = _data.Update(cosmic_Objects);
                    Printer.PrintInfo(cosmic_Objects);
                    break;

                case UserMonitor.Command.print_obj:                                               // Печатаем объект
                    Printer.PrintCurrentType(cosmic_Objects, str);
                    break;

                case UserMonitor.Command.Serialize:                                               // Сериализуем в файл определнный объект
                    _xml.WriteToXml(cosmic_Objects, str);
                    break;

                case UserMonitor.Command.Desirialize:                                             // Десириализует объекты из файла
                    _xml.ReadToXml(ref cosmic_Objects);
                    _data.Rewrite(cosmic_Objects);
                    break;

                case UserMonitor.Command.back_up:                                                 // Возвращаем первоначальные данные
                    _values.CreateData(cosmic_Objects);
                    _data.Rewrite(cosmic_Objects);
                    cosmic_Objects = _data.Update(cosmic_Objects);
                    Console.WriteLine("Система восстановлена!");
                    break;

                case UserMonitor.Command.Fail:                                                    // Ошибка введеных данных
                    Console.WriteLine("Неверно введены данные!");
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cosmic_Objects"></param>
        public void CreatePlanet(string key, List<object> cosmic_Objects)
        {

            object cosmic_Objects1 = null;
            var state = AddingState.Start;
            while (state != AddingState.Success)
            {

                cosmic_Objects1 = _add.AddPlanet(_user.GetQueryPlanet(), out state);
                if (state == AddingState.Fail)
                {
                    Console.WriteLine("Неверно введены данные для {0}", key.Substring(4));
                }
            }
            cosmic_Objects.Add(cosmic_Objects1);
            _data.AddPlanet(cosmic_Objects1);

            Console.WriteLine("Объект добавлен!");

        }

        public void CreateStar(string key, List<object> cosmic_Objects)
        {

            object cosmic_Objects1 = null;
            var state = AddingState.Start;
            while (state != AddingState.Success)
            {

                cosmic_Objects1 = _add.AddStar(_user.GetQueryStar(), out state);
                if (state == AddingState.Fail)
                {
                    Console.WriteLine("Неверно введены данные для {0}", key.Substring(4));
                }
            }
            cosmic_Objects.Add(cosmic_Objects1);
            _data.AddStar(cosmic_Objects1);

            Console.WriteLine("Объект добавлен!");

        }

        public void CreateAsteroid(string key, List<object> cosmic_Objects)
        {

            object cosmic_Objects1 = null;
            var state = AddingState.Start;
            while (state != AddingState.Success)
            {

                cosmic_Objects1 = _add.AddAsteroid(_user.GetQueryAsteroid(), out state);
                if (state == AddingState.Fail)
                {
                    Console.WriteLine("Неверно введены данные для {0}", key.Substring(4));
                }
            }
            cosmic_Objects.Add(cosmic_Objects1);
            _data.AddAsteroid(cosmic_Objects1);

            Console.WriteLine("Объект добавлен!");

        }

        public void CreateBlack_hole(string key, List<object> cosmic_Objects)
        {

            object cosmic_Objects1 = null;
            var state = AddingState.Start;
            while (state != AddingState.Success)
            {

                cosmic_Objects1 = _add.AddBlack_hole(_user.GetQueryBlack_hole(), out state);
                if (state == AddingState.Fail)
                {
                    Console.WriteLine("Неверно введены данные для {0}", key.Substring(4));
                }
            }
            cosmic_Objects.Add(cosmic_Objects1);
            _data.AddBlack_hole(cosmic_Objects1);

            Console.WriteLine("Объект добавлен!");

        }

        public void Delete(string key, List<object> cosmic_Objects)
        {

            var state = AddingState.Start;
            state = _del.DeleteObj(key, cosmic_Objects);
            if (state == AddingState.Fail)
            {
                Console.WriteLine($"Объекта с таким Id нет или неверно введены данные!");
            }
            if (state == AddingState.Success)
            {
                _data.DeleteObj(key);
                Console.WriteLine("Объект удален!");
            }
        }




    }
}



