﻿// Все запросы в Базу Данных должны быть реализованы через репозиторий
namespace Cosmic_objects
{
    public class QueriesDB
    {

        // private AppSettings _settings;
        private Repos _repos;


        public QueriesDB(Repos settings)
        {
            _repos = settings;
        }


        // Добавление объекта Планеты в БД
        public void AddPlanet(object planet)
        {
            _repos.Execute(
                @"INSERT INTO planet (name , radius, size, speed) VALUES (@Name, @Radius, @Size, @Speed_Planet)", planet);
        }
        // Добавление объекта Звезды в БД
        public void AddStar(object star)
        {
            _repos.Execute(
                "INSERT INTO star (name , radius, size, distance_to_the_star)VALUES(@Name, @Radius, @Size, @Distance_to_the_star)", star);
        }
        // Добавление объекта Астероида в БД
        public void AddAsteroid(object asteroid)
        {
            _repos.Execute(
                "INSERT INTO asteroid (name , radius, size, speed)VALUES(@Name, @Radius, @Size, @Speed_Asteroid)", asteroid);
        }
        // Добавление объекта Черной Дыры в БД
        public void AddBlack_hole(object black_hole)
        {
            _repos.Execute(
                 "INSERT INTO black_hole (name , radius, size, opening_year)VALUES(@Name, @Radius, @Size, @Opening_Year)", black_hole);
        }

        // Удаление объекта из БД
        public void DeleteObj(string str)
        {
            AddingState state = AddingState.Start;
            string[] parameters = str.Split(' ', StringSplitOptions.RemoveEmptyEntries);


            if (parameters[1].Equals("P", StringComparison.OrdinalIgnoreCase))
            {
                _repos.Execute(
              $"DELETE FROM planet WHERE planetid={parameters[2]}");

            }
            else if (parameters[1].Equals("S", StringComparison.OrdinalIgnoreCase))
            {
                _repos.Execute(
              $"DELETE FROM star WHERE starid={parameters[2]}");


            }
            else if (parameters[1].Equals("A", StringComparison.OrdinalIgnoreCase))
            {
                _repos.Execute(
              $"DELETE FROM asteroid WHERE asteroidid={parameters[2]}");

            }
            else if (parameters[1].Equals("B", StringComparison.OrdinalIgnoreCase))
            {
                _repos.Execute(
              $"DELETE FROM black_hole WHERE black_holeid={parameters[2]}");

            }


        }

        public List<object> Update(List<object> list)
        {

            list.Clear();
            var result_Planet = _repos.Query<Planet>(
                "SELECT * FROM planet ORDER BY planetid asc");
            var result_Star = _repos.Query<Star>(
                "SELECT * FROM star ORDER BY starid asc");
            var result_Asteroid = _repos.Query<Asteroid>(
                "SELECT * FROM asteroid ORDER BY asteroidid asc");
            var result_Black_hole = _repos.Query<Black_hole>(
                "SELECT * FROM black_hole ORDER BY black_holeid asc");
            list.AddRange(result_Planet);
            list.AddRange(result_Star);
            list.AddRange(result_Asteroid);
            list.AddRange(result_Black_hole);

            return list;
        }

        public void Rewrite(List<object> cosmic_Objects)
        {
            bool state_1 = false, state_2 = false, state_3 = false, state_4 = false;

            foreach (object obj in cosmic_Objects)
            {
                if (obj is Planet) state_1 = true;
                if (obj is Star) state_2 = true;
                if (obj is Asteroid) state_3 = true;
                if (obj is Black_hole) state_4 = true;

            }
            //Удаляет все данные
            if (state_1) _repos.Execute(
                "DELETE FROM planet");
            if (state_2) _repos.Execute(
                "DELETE FROM star");
            if (state_3) _repos.Execute(
                "DELETE FROM asteroid");
            if (state_4) _repos.Execute(
                "DELETE FROM black_hole");

            //Записывает полученные новые
            foreach (var obj in cosmic_Objects)
            {
                if (obj is Planet) _repos.Execute(
                    "INSERT INTO planet (name , radius, size, speed)VALUES(@Name, @Radius, @Size, @Speed_Planet)", obj);
                if (obj is Star) _repos.Execute(
                    "INSERT INTO Star (name , radius, size, distance_to_the_star)VALUES(@Name, @Radius, @Size, @Distance_to_the_star)", obj);
                if (obj is Asteroid) _repos.Execute(
                    "INSERT INTO Asteroid (name , radius, size, speed)VALUES(@Name, @Radius, @Size, @Speed_Asteroid)", obj);
                if (obj is Black_hole) _repos.Execute(
                    "INSERT INTO black_hole (name , radius, size, opening_Year)VALUES(@Name, @Radius, @Size, @Opening_Year)", obj);
            }


        }


    }
}
