﻿using Microsoft.Extensions.Configuration;


namespace Cosmic_objects
{

    public class AppSettings
    {
        // private IConfigurationRoot config;
        public string ConnectionString;
        public AppSettings(IConfigurationRoot config)
        {
            ConnectionString = config.GetConnectionString("DBTest");
        }
    }
}
