﻿

namespace Cosmic_objects
{
    public class Data
    {
        public void CreateData(List<object> cosmic_Objects)
        {


            cosmic_Objects.Clear();
            // Космические объекты: Планеты
            cosmic_Objects.Add(new Planet() { Name = "Earth", Size = 12742, Speed_Planet = 29.765, Radius = 6371 });
            cosmic_Objects.Add(new Planet() { Name = "Mars", Size = 6779, Speed_Planet = 24.13, Radius = 3390 });
            cosmic_Objects.Add(new Planet() { Name = "Venus", Size = 12104, Speed_Planet = 35.0, Radius = 6052 });
            cosmic_Objects.Add(new Planet() { Name = "Jupiter", Size = 139820, Speed_Planet = 13.07, Radius = 69911 });

            // Космические объекты: Звезды
            cosmic_Objects.Add(new Star() { Name = "Aldebaran", Size = 61.402, Distance_to_the_star = 68, Radius = 30701000 });
            cosmic_Objects.Add(new Star() { Name = "Altair", Size = 2.5323, Distance_to_the_star = 360, Radius = 1266200 });
            cosmic_Objects.Add(new Star() { Name = "Betelgeize", Size = 617.1, Distance_to_the_star = 650, Radius = 617100000 });
            cosmic_Objects.Add(new Star() { Name = "Vega", Size = 1.6432, Distance_to_the_star = 27, Radius = 1643200 });

            // Космические объекты: Астероиды
            cosmic_Objects.Add(new Asteroid() { Name = "Pallas", Size = 515, Speed_Asteroid = 17.645, Radius = 272 });
            cosmic_Objects.Add(new Asteroid() { Name = "Juno", Size = 234, Speed_Asteroid = 17.919, Radius = 135 });
            cosmic_Objects.Add(new Asteroid() { Name = "Vesta", Size = 525.4, Speed_Asteroid = 19.346, Radius = 262 });
            cosmic_Objects.Add(new Asteroid() { Name = "Astraea", Size = 106.7, Speed_Asteroid = 18.394, Radius = 53 });

            // Космические объекты: Черных Дыры
            cosmic_Objects.Add(new Black_hole() { Name = "TON 618", Size = 720, Opening_Year = 1957, Radius = 20043432 });
            cosmic_Objects.Add(new Black_hole() { Name = "IC 1101", Size = 450, Opening_Year = 1790, Radius = 1956900 });
            cosmic_Objects.Add(new Black_hole() { Name = "S5 0014+81", Size = 430, Opening_Year = 1981, Radius = 1343545 });
            cosmic_Objects.Add(new Black_hole() { Name = "SDSS J102325.31+514251.0", Size = 370, Opening_Year = 1957, Radius = 1255465 });

        }
        public List<object> SomeSpaceObjects()
        {
            List<object> list = new List<object>();
            list.Add(new Planet() { PlanetID = 1, Name = "Test_1", Size = 35343, Speed_Planet = 35.6654, Radius = 323423 });
            list.Add(new Planet() { PlanetID = 2, Name = "Test_1", Size = 324, Speed_Planet = 25.46, Radius = 654 });
            list.Add(new Planet() { PlanetID = 3, Name = "Test_1", Size = 7656, Speed_Planet = 37.0, Radius = 32325 });
            list.Add(new Planet() { PlanetID = 4, Name = "Test_1", Size = 435654, Speed_Planet = 13.07, Radius = 69911 });

            // Космические объекты: Звезды
            list.Add(new Star() { StarID = 1, Name = "Test_2", Size = 545, Distance_to_the_star = 234, Radius = 30705435 });
            list.Add(new Star() { StarID = 2, Name = "Test_2", Size = 2.653, Distance_to_the_star = 234, Radius = 543543345 });
            list.Add(new Star() { StarID = 3, Name = "Test_2", Size = 654.4, Distance_to_the_star = 53, Radius = 5435435 });
            list.Add(new Star() { StarID = 4, Name = "Test_2", Size = 1.43534, Distance_to_the_star = 2, Radius = 7657565 });

            // Космические объекты: Астероиды
            list.Add(new Asteroid() { AsteroidID = 1, Name = "Test_3", Size = 543, Speed_Asteroid = 19.4353, Radius = 43 });
            list.Add(new Asteroid() { AsteroidID = 2, Name = "Test_3", Size = 543, Speed_Asteroid = 35.543, Radius = 543 });
            list.Add(new Asteroid() { AsteroidID = 3, Name = "Test_3", Size = 765.6, Speed_Asteroid = 65.5435, Radius = 65 });
            list.Add(new Asteroid() { AsteroidID = 4, Name = "Test_3", Size = 124.6, Speed_Asteroid = 37.543, Radius = 676 });

            // Космические объекты: Черных Дыры
            list.Add(new Black_hole() { Black_holeID = 1, Name = "Test_4", Size = 800, Opening_Year = 1245, Radius = 32432532 });
            list.Add(new Black_hole() { Black_holeID = 2, Name = "Test_4", Size = 43, Opening_Year = 1345, Radius = 76576565 });
            list.Add(new Black_hole() { Black_holeID = 3, Name = "Test_4", Size = 554, Opening_Year = 1467, Radius = 2342355 });
            list.Add(new Black_hole() { Black_holeID = 4, Name = "Test_4", Size = 657, Opening_Year = 1834, Radius = 65768756 });

            return list;
        }

    }
}
