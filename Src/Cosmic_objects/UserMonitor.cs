﻿namespace Cosmic_objects
{
    public class UserMonitor
    {
        public string GetQueryPlanet()
        {
            Console.WriteLine("Введите Название, Размер, Скорость Планеты и ее Радиус :");
            string value = Console.ReadLine();
            return value;
        }

        public string GetQueryStar()
        {
            Console.WriteLine("Введите Название, Размер, Растояние до Звезды и ее Радиус :");
            string value = Console.ReadLine();
            return value;
        }

        public string GetQueryAsteroid()
        {
            Console.WriteLine("Введите Название, Размер, Скорость и Радиус Астероида :");
            string value = Console.ReadLine();
            return value;
        }

        public string GetQueryBlack_hole()
        {
            Console.WriteLine("Введите Название, Размер, Год Открытия и Радиус Черной дыры :");
            string value = Console.ReadLine();
            return value;
        }
        public Command Query(out string str)
        {
            Command command;
            Console.WriteLine("Введите команду:");
            str = Console.ReadLine();

            if (str.Equals("Add P", StringComparison.OrdinalIgnoreCase))
                command = Command.Add_planet;

            else if (str.Equals("Add S", StringComparison.OrdinalIgnoreCase))
                command = Command.Add_star;

            else if (str.Equals("Add A", StringComparison.OrdinalIgnoreCase))
                command = Command.Add_asteroid;

            else if (str.Equals("Add B", StringComparison.OrdinalIgnoreCase))
                command = Command.Add_black_hole;

            else if (str.Equals("Pr", StringComparison.OrdinalIgnoreCase))
                command = Command.print;

            else if (str.Equals("Pr P", StringComparison.OrdinalIgnoreCase))
                command = Command.print_obj;

            else if (str.Equals("Pr S", StringComparison.OrdinalIgnoreCase))
                command = Command.print_obj;

            else if (str.Equals("Pr A", StringComparison.OrdinalIgnoreCase))
                command = Command.print_obj;

            else if (str.Equals("Pr B", StringComparison.OrdinalIgnoreCase))
                command = Command.print_obj;

            else if (str.Equals("Sr P", StringComparison.OrdinalIgnoreCase))
                command = Command.Serialize;

            else if (str.Equals("Sr S", StringComparison.OrdinalIgnoreCase))
                command = Command.Serialize;

            else if (str.Equals("Sr A", StringComparison.OrdinalIgnoreCase))
                command = Command.Serialize;

            else if (str.Equals("Sr B", StringComparison.OrdinalIgnoreCase))
                command = Command.Serialize;

            else if (str.Equals("Ds", StringComparison.OrdinalIgnoreCase))
                command = Command.Desirialize;

            else if (str.StartsWith("Del", StringComparison.OrdinalIgnoreCase))
                command = Command.del;

            else if (str.Equals("BU", StringComparison.OrdinalIgnoreCase))
                command = Command.back_up;

            else
                command = Command.Fail;

            return command;
        }
        public enum Command
        {
            Add_planet,
            Add_star,
            Add_asteroid,
            Add_black_hole,
            del,
            print,
            print_obj,
            Serialize,
            Desirialize,
            back_up,
            Fail
        }
    }
}
