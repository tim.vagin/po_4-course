﻿using Microsoft.Extensions.Configuration;

namespace Cosmic_objects
{
    class Program
    {

        static void Main()
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
               .Build();

            var settings = new AppSettings(config);
            var repos = new Repos(settings);
            var values = new Data();
            var add = new Addition();
            var del = new Deletion();
            var Xml = new XmlMethods(values);
            var db = new QueriesDB(repos);
            var user = new UserMonitor();
            var comand = new Comand(db, add, del, values, Xml, user);


            // Предустанавливаем значения в Xml Файл
            Xml.CreateDataInXmlFile();

            //Cоздаем List 
            List<object> cosmic_Objects = new List<object>();


            // Считываем данные с БД и записываем в List объекты
            cosmic_Objects = db.Update(cosmic_Objects);
            string str;
            //Выводим на печать объекты и их значения с базы данных
            Printer.PrintInfo(cosmic_Objects);
            bool working = true;
            // Обработчик команд

            while (working)
            {
                UserMonitor.Command query = user.Query(out str);

                comand.Comand_Handler(query, str, cosmic_Objects);
            }

        }

    }
}
