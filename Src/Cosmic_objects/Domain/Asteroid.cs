﻿namespace Cosmic_objects
{

    public class Asteroid
    {
        public int AsteroidID { get; set; }

        private string name;
        private int radius;
        private double size;
        private double speed;

        // Доступ к характеристикам через свойства класса


        public string Name
        {
            get { return name; }
            set { name = value; }

        }

        public int Radius
        {
            get { return radius; }
            set { radius = value; }

        }
        public double Size
        {
            get { return size; }
            set { size = value; }

        }


        public double Speed_Asteroid
        {
            get { return speed; }
            set { speed = value; }

        }
    }
}
