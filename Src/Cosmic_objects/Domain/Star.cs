﻿namespace Cosmic_objects
{

    public class Star
    {
        public int StarID { get; set; }
        private int distance_to_the_star;
        private string name;
        private int radius;
        private double size;

        // Доступ к характеристикам через свойства класса


        public string Name
        {
            get { return name; }
            set { name = value; }

        }

        public int Radius
        {
            get { return radius; }
            set { radius = value; }

        }
        public double Size
        {
            get { return size; }
            set { size = value; }

        }

        public int Distance_to_the_star
        {
            get { return distance_to_the_star; }
            set { distance_to_the_star = value; }
        }
    }
}
