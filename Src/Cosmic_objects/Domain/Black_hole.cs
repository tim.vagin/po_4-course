﻿namespace Cosmic_objects
{

    public class Black_hole
    {
        public int Black_holeID { get; set; }
        private string name;
        private int radius;
        private double size;
        private int opening_year;

        // Доступ к характеристикам через свойства класса


        public string Name
        {
            get { return name; }
            set { name = value; }

        }

        public int Radius
        {
            get { return radius; }
            set { radius = value; }

        }
        public double Size
        {
            get { return size; }
            set { size = value; }


        }


        public int Opening_Year
        {
            get { return opening_year; }
            set { opening_year = value; }

        }
    }
}
