﻿namespace Cosmic_objects
{

    public class Planet
    {
        public int PlanetID { get; set; }

        private double speed;
        private string name;
        private int radius;
        private double size;

        // Доступ к характеристикам через свойства класса


        public string Name
        {
            get { return name; }
            set { name = value; }

        }

        public int Radius
        {
            get { return radius; }
            set { radius = value; }

        }
        public double Size
        {
            get { return size; }
            set { size = value; }



        }
        public double Speed_Planet
        {
            get { return speed; }
            set { speed = value; }


        }
    }
}
