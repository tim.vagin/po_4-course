﻿namespace Cosmic_objects
{
    internal class Printer
    {
        public static void PrintInfo(List<object> cosmic_Objects)
        {

            Console.WriteLine("\n\tИнформационная система о космических объектах: Звёзды, Планеты, Астеройды, Чёрные дыры.");
            // Выводим на печать Планеты
            Console.WriteLine("\n Планеты(Planet):");
            Console.WriteLine(new string('-', 100));
            foreach (object planets in cosmic_Objects)
            {
                Planet planet = planets as Planet;
                if (planet != null)

                    Console.WriteLine($"Id: " + planet.PlanetID.ToString().PadRight(4) + "Планета: " + planet.Name.PadRight(14).Substring(0, 14) + ' ' + "Размер: " + planet.Size.ToString().PadRight(10).Substring(0, 10) + ' ' + "км" +
                        " Скорость планеты: " + planet.Speed_Planet.ToString().PadRight(9).Substring(0, 9) + ' ' + "км" + '/' + "c " + "Радиус: " + planet.Radius.ToString().PadRight(8).Substring(0, 8) + ' ' + " км");
            }
            Console.WriteLine(new string('-', 100));

            // Выводим на печать Звезды
            Console.WriteLine("\n Звезды(Star):");
            Console.WriteLine(new string('-', 100));
            foreach (object stars in cosmic_Objects)
            {
                Star star = stars as Star;
                if (star != null)
                    Console.WriteLine($"Id: " + star.StarID.ToString().PadRight(4) + "Звезда: " + star.Name.PadRight(15).Substring(0, 15) + ' ' + "Размер: " + star.Size.ToString().PadRight(9).Substring(0, 9) + ' ' + " млн км " +
                        " Расстояние до звезды: " + star.Distance_to_the_star.ToString().PadRight(5).Substring(0, 5) + ' ' + " световых лет " + "Радиус: " + star.Radius.ToString().PadRight(9).Substring(0, 9) + ' ' + " км");
            }
            Console.WriteLine(new string('-', 100));

            // Выводим на печать Астероиды
            Console.WriteLine("\n Астероиды(Asteroid):");
            Console.WriteLine(new string('-', 100));
            foreach (object asteroids in cosmic_Objects)
            {
                Asteroid asteroid = asteroids as Asteroid;
                if (asteroid != null)
                    Console.WriteLine($"Id: " + asteroid.AsteroidID.ToString().PadRight(4) + "Астероид: " + asteroid.Name.PadRight(13).Substring(0, 13) + ' ' + "Размер: " + asteroid.Size.ToString().PadRight(9).Substring(0, 9) + ' ' + "км " +
                        "Орбитальная скорость: " + asteroid.Speed_Asteroid.ToString().PadRight(9).Substring(0, 9) + ' ' + "км" + '/' + "c " + "Радиус: " + asteroid.Radius.ToString().PadRight(5).Substring(0, 5) + ' ' + " км");
            }
            Console.WriteLine(new string('-', 100));
            // Выводим на печать Черные дыры
            Console.WriteLine("\n Черные дыры(Black_hole):");
            Console.WriteLine(new string('-', 100));
            foreach (object black_holes in cosmic_Objects)
            {
                Black_hole black_hole = black_holes as Black_hole;
                if (black_hole != null)
                    Console.WriteLine($"Id: " + black_hole.Black_holeID.ToString().PadRight(4) + "Черная дыра: " + black_hole.Name.PadRight(24).Substring(0, 14) + ' ' + "Размер: " + black_hole.Size.ToString().PadRight(5).Substring(0, 5) + ' ' + " млн км " +
                        " Год открытия: " + black_hole.Opening_Year.ToString().PadRight(5).Substring(0, 4) + ' ' + "год " + "Радиус: " + black_hole.Radius.ToString().PadRight(5).Substring(0, 5) + ' ' + "млн км");
            }
            Console.WriteLine(new string('-', 100));

            Console.WriteLine("Список  команд: \nPr - выводит информацию на печать " +
                "\nPr + (тип объекта -- Первая буква --) - выводит информацию на печать конкретного объекта" +
                "\nAdd + (тип объекта -- Первая буква --) - добавляет объект указанного типа в информационную систему " +
                "\nDel + (тип объекта -- Первая буква --) + (Id объекта) - удаляет объект по указанному Id и типу из системы " +
                "\nSr + (тип объекта -- Первая буква --) - команда сериализует объект данного типа в форматe xml" +
                "\nDs - команда считывает информацию с файла и обновляет информационную систему " +
                "\nBu - Сбрасывает все и возвращает первоначальные данные системы");

        }

        public static void PrintCurrentType(List<object> cosmic_Objects, string str)
        {
            if (str.Equals("Pr P", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine(new string('-', 100));
                var selectedObj = cosmic_Objects.Where(p => p is Planet);
                foreach (Planet planet in selectedObj)
                {
                    Console.WriteLine($" Планета: " + planet.Name.PadRight(14).Substring(0, 14) + ' ' + "Размер: " + planet.Size.ToString().PadRight(10).Substring(0, 10) + ' ' + "км" +
                        " Скорость планеты: " + planet.Speed_Planet.ToString().PadRight(9).Substring(0, 9) + ' ' + "км" + '/' + "c " + "Радиус: " + planet.Radius.ToString().PadRight(8).Substring(0, 8) + ' ' + " км");
                }
                Console.WriteLine(new string('-', 100));
            }
            else if (str.Equals("Pr S", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine(new string('-', 100));
                var selectedObj = cosmic_Objects.Where(p => p is Star);
                foreach (Star star in selectedObj)
                {
                    Console.WriteLine($" Звезда: " + star.Name.PadRight(15).Substring(0, 15) + ' ' + "Размер: " + star.Size.ToString().PadRight(9).Substring(0, 9) + ' ' + " млн км " +
                        " Расстояние до звезды: " + star.Distance_to_the_star.ToString().PadRight(5).Substring(0, 5) + ' ' + " световых лет " + "Радиус: " + star.Radius.ToString().PadRight(9).Substring(0, 9) + ' ' + " км");

                }
                Console.WriteLine(new string('-', 100));
            }
            else if (str.Equals("Pr A", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine(new string('-', 100));
                var selectedObj = cosmic_Objects.Where(p => p is Asteroid);
                foreach (Asteroid asteroid in selectedObj)
                {
                    Console.WriteLine($" Астероид: " + asteroid.Name.PadRight(13).Substring(0, 13) + ' ' + "Размер: " + asteroid.Size.ToString().PadRight(9).Substring(0, 9) + ' ' + "км " +
                        "Орбитальная скорость: " + asteroid.Speed_Asteroid.ToString().PadRight(9).Substring(0, 9) + ' ' + "км" + '/' + "c " + "Радиус: " + asteroid.Radius.ToString().PadRight(5).Substring(0, 5) + ' ' + " км");

                }
                Console.WriteLine(new string('-', 100));
            }
            else if (str.Equals("Pr B", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine(new string('-', 100));
                var selectedObj = cosmic_Objects.Where(p => p is Black_hole);
                foreach (Black_hole black_hole in selectedObj)
                {
                    Console.WriteLine($" Черная дыра: " + black_hole.Name.PadRight(24).Substring(0, 14) + ' ' + "Размер: " + black_hole.Size.ToString().PadRight(5).Substring(0, 5) + ' ' + " млн км " +
                        " Год открытия: " + black_hole.Opening_Year.ToString().PadRight(5).Substring(0, 4) + ' ' + "год " + "Радиус: " + black_hole.Radius.ToString().PadRight(5).Substring(0, 5) + ' ' + "млн км");

                }
                Console.WriteLine(new string('-', 100));
            }

        }
    }
}
