﻿

namespace Cosmic_objects
{
    public class Deletion
    {

        // Удаляет объект с указанным именем и возвращает Success, иначе возвращает Fail
        public AddingState DeleteObj(string str, List<object> cosmic_Objects)
        {
            AddingState state = AddingState.Start;

            string[] parameters = str.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            if (parameters.Length != 3)
            {
                state = AddingState.Fail;
                return state;
            }
            else
            {
                for (int i = 0; i < cosmic_Objects.Count; i++)
                {


                    if (cosmic_Objects[i] is Planet && parameters[1].Equals("P", StringComparison.OrdinalIgnoreCase))
                    {
                        Planet planet = (Planet)cosmic_Objects[i];
                        if (planet.PlanetID.ToString() == parameters[2])
                        {
                            cosmic_Objects.RemoveAt(i);
                            state = AddingState.Success;
                            return state;
                        }
                    }
                    else if (cosmic_Objects[i] is Star && parameters[1].Equals("S", StringComparison.OrdinalIgnoreCase))
                    {
                        Star star = (Star)cosmic_Objects[i];
                        if (star.StarID.ToString() == parameters[2])
                        {
                            cosmic_Objects.RemoveAt(i);
                            state = AddingState.Success;
                            return state;
                        }
                    }
                    else if (cosmic_Objects[i] is Asteroid && parameters[1].Equals("A", StringComparison.OrdinalIgnoreCase))
                    {
                        Asteroid asteroid = (Asteroid)cosmic_Objects[i];
                        if (asteroid.AsteroidID.ToString() == parameters[2])
                        {
                            cosmic_Objects.RemoveAt(i);
                            state = AddingState.Success;
                            return state;
                        }
                    }
                    else if (cosmic_Objects[i] is Black_hole && parameters[1].Equals("B", StringComparison.OrdinalIgnoreCase))
                    {
                        Black_hole black_hole = (Black_hole)cosmic_Objects[i];
                        if (black_hole.Black_holeID.ToString() == parameters[2])
                        {
                            cosmic_Objects.RemoveAt(i);
                            state = AddingState.Success;
                            return state;
                        }
                    }
                }

                if (state == AddingState.Start)
                    state = AddingState.Fail;
            }


            return state;
        }

    }
}
