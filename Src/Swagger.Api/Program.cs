using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
    {
        options.SwaggerDoc("v1",
        new Microsoft.OpenApi.Models.OpenApiInfo
        {
            Title = "Swagger Demo API",
            Version = "v1",
            Description = "Demo API for show Swagger",
        });
        options.SwaggerDoc("v2",
        new Microsoft.OpenApi.Models.OpenApiInfo
        {
            Title = "Swagger Demo API",
            Version = "2",
            Description = "Demo API for show Swagger"
        });

        var filename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        var filePath = Path.Combine(AppContext.BaseDirectory, filename);
        options.IncludeXmlComments(filePath);

    });

builder.Services.AddOpenApiDocument();
//builder.Services.AddSwaggerDocument();

builder.Services.AddScoped<Swagger.Api.Data>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseOpenApi();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v1/Swagger.json", "PhoneStore Страница 1");
        options.SwaggerEndpoint("/swagger/v2/Swagger.json", "PhoneStore Страница 2");
        options.HeadContent = "PHONESTORE";
        
    });
}

app.UseReDoc();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();


app.Run();



