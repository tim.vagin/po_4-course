﻿using Microsoft.Extensions.Configuration;


namespace Swagger.Api
{

    public class AppSettings
    {
        // private IConfigurationRoot config;
        public string ConnectionString;
        public AppSettings(IConfigurationRoot config)
        {
            ConnectionString = config.GetConnectionString("DBTest");
        }
    }
}
