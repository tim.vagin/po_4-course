using Microsoft.AspNetCore.Mvc;
using Dapper;

namespace Swagger.Api
{   /// <summary>
/// ���������� ����������� �� ������ Get/Post
/// </summary>
    [ApiController]
    [Route("[controller]")]
    public class PhoneController : ControllerBase
    {
        public Data _data;
        
        private readonly ILogger<PhoneController> _logger;

        public PhoneController(ILogger<PhoneController> logger, Data data)
        {
            _logger = logger;
            _data = data;
         
        }

        /// <summary>
        /// ���������� ������� � ���� ��
        /// </summary>
        /// <returns> ���������� �������</returns>
        [HttpGet(Name = "GetPhone")]
        public IEnumerable<Phone> Get()
        {

            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
               .Build();

            var settings = new AppSettings(config);

            IList<Phone> phoneList = new List<Phone>();


                ConnectionFactory factory = new ConnectionFactory(settings.ConnectionString);
                using (var connection = factory.Create())
                {
                    var result = connection.Query<Phone>("SELECT * FROM phone ORDER BY id asc");
                    phoneList = result.ToList();
                }

            return phoneList;
            
        }

        /// <summary>
        /// ��������� ����� ������
        /// </summary>
        /// <returns> ���������� ���������</returns>
        [HttpPost(Name = "PostPhone")]
        public void Post(string Name, string Company, int Price)
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
               .Build();

            var settings = new AppSettings(config);

             Phone obj = new Phone() {Name = Name, Company = Company, Price = Price };

            ConnectionFactory factory = new ConnectionFactory(settings.ConnectionString);
            using (var connection = factory.Create())
            {
                var result = connection.Execute("INSERT INTO phone (name , company, price)VALUES(@Name, @Company, @Price)", obj);
            }

        }

        /// <summary>
        /// ������� ����� ������
        /// </summary>
        /// <returns>���������� ��������� </returns>
        [HttpDelete(Name = "DeletePhone")]
        public void Delete(int Id)
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
               .Build();

            var settings = new AppSettings(config);

            ConnectionFactory factory = new ConnectionFactory(settings.ConnectionString);
            using (var connection = factory.Create())
            {
                var result = connection.Execute($"DELETE FROM phone WHERE id={Id}");
            }

        }

    }
}


// phoneList = _data.Datainfo;

//foreach (var obj in phoneList)
//{

//    ConnectionFactory factory = new ConnectionFactory(settings.ConnectionString);
//    using (var connection = factory.Create())
//    {
//        var result = connection.Execute("INSERT INTO phone (name , company, price)VALUES(@Name, @Company, @Price)", obj);
//    }

//}

// public IEnumerable<T> Query<T>(string sql)