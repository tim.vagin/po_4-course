﻿namespace Swagger.Api
{
    public class Data
    {

        private static readonly string[] Companies = new[]
         {
        "Microsoft", "Google", "Yota", "Tele2", "Yandex", "Huawei"
        };

        private static readonly string[] Names = new[]
         {
        "Samsung 20", "Nokia 6.1", "IPhone 13", "Mi8", "Redmi Note 2", "Motorolla 6"
        };

        private IList<Phone> datainfo = new List<Phone>(Enumerable.Range(1, 5).Select(index => new Phone
        {
               Name = Names[Random.Shared.Next(Names.Length)],
               Price = Random.Shared.Next(2000, 45000),
               Company = Companies[Random.Shared.Next(Companies.Length)]
    })
            );

        public IList<Phone> Datainfo
        {
            get { return datainfo; }
            set { datainfo = value; }

        }

        public Data()
        {
           
        }

    }
}
