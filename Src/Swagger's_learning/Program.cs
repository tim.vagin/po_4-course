﻿namespace Swagger_s_learning
{
    public class Program
    {
        public static void Main()
        {
            /// <remarks>
            /// Создаем 3 сущности, но не более
            /// </remarks>
            Person Ivan =  Person.getPerson("Ivan", 21, new DateTime(1994, 11, 25));
            Person Max = Person.getPerson("Max", 23, new DateTime(2000, 5, 15));
            Person Lera = Person.getPerson("Lera", 10, new DateTime(1998, 2, 7));
            // Person Dima = Person.getPerson("Ivan", 21, new DateTime(1994, 11, 25));
            List<Person> list = new List<Person>() { Ivan, Max, Lera };

            Printer printer = new Printer();
            printer.PrintInfo(list);
           
            Console.ReadKey();
        }
    }
}