﻿

namespace Swagger_s_learning
{
    public class Person
    {
        private static Person person = null;
        private string name;
        private int age;
        private DateTime birthday;

        public string Name { get { return name; } set { name = value; } }
        public int Age { get { return age; } set { age = value; } }
        public DateTime Birthday { get { return birthday; } set { birthday = value; } }
            
        private  static int count = 0;

        protected Person(string name, int age, DateTime birthday) { 
            this.name = name;
            this.age = age;
            this.birthday = birthday;
            count++; }

       static public Person getPerson(string name, int age, DateTime birthday)
        {
            if (count < 3)
                person = new Person(name, age, birthday);
            else
                Console.WriteLine("Попытка создания больше 3 сущностей. Отказано");
                return person;
        }
    }
}
