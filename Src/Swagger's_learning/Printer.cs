﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swagger_s_learning
{
    public class Printer
    {
       public void PrintInfo(List<Person> list)
        {
            foreach (Person person in list)
                Console.WriteLine("Name : {0}  Age : {1}  Birthday : {2} ", person.Name , person.Age, person.Birthday.Date.ToShortDateString());
        }
    }
}
