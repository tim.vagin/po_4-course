using Cosmic_objects;
using NUnit.Framework;
using System.Collections.Generic;

namespace Cosmic_objectsTest
{
    public class Cosmic_objectsTest
    {
        Addition _add;
        Deletion _del;
        XmlMethods _xmlMethods;
        Data _data;
        public Cosmic_objectsTest()
        {
            _add = new Addition();
            _del = new Deletion();
            _data = new Data();
            _xmlMethods = new XmlMethods(_data);
        }

        [Test]
        public void Get_String_Success_and_Planet_Returned()
        {
            // ������� �� ����
            string Versus = "Versus 3050 2305 2000";
            // ��� ������� ��������
            Planet planet1 = new Planet() { Name = "Versus", Size = 3050, Speed_Planet = 2305, Radius = 2000 };
            AddingState state1 = AddingState.Success;


            // ��� ��������
            AddingState state2;
            object actual_Planet = _add.AddPlanet(Versus, out state2);
            Planet planet2 = actual_Planet as Planet;
            // ����������
            Assert.AreEqual(state1, state2);
            Assert.AreEqual(planet1.Name, planet2.Name);
            Assert.AreEqual(planet1.Size, planet2.Size);
            Assert.AreEqual(planet1.Speed_Planet, planet2.Speed_Planet);
            Assert.AreEqual(planet1.Radius, planet2.Radius);
        }
        [Test]
        public void Get_String_Fail_and_Planet_as_null_Returned()
        {
            // ������� �� ����
            string Versus = "Versus 3050 2000";
            // ��� ������� ��������
            Planet planet1 = null;
            AddingState state1 = AddingState.Fail;


            // ��� ��������
            AddingState state2;
            object actual_Planet = _add.AddPlanet(Versus, out state2);

            Planet planet2 = actual_Planet as Planet;
            // ����������
            Assert.AreEqual(state1, state2);
            Assert.AreEqual(planet1, planet2);

        }
        [Test]
        public void Get_Delete_Name_Succes_Returned()
        {
            // ������ �� ����
            string Name = "Delete S 2 ";
            List<object> list = new List<object>();
            list.Add(new Planet() { PlanetID = 1, Name = "Versus", Radius = 345, Size = 34.54, Speed_Planet = 666 });
            list.Add(new Star() { StarID = 2, Name = "Moon", Radius = 765, Size = 55.55, Distance_to_the_star = 666 });

            // ��� ������� ��������
            AddingState expected = AddingState.Success;

            // ��� ��������
            AddingState actual = _del.DeleteObj(Name, list);

            // ����������
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void Get_Delete_Name_Fail_Returned()
        {
            // ������ �� ����
            string Name = "Delete S 3";
            List<object> list = new List<object>();
            list.Add(new Planet() { PlanetID = 1, Name = "Versus", Radius = 345, Size = 34.54, Speed_Planet = 666 });
            list.Add(new Star() { StarID = 2, Name = "Sun", Radius = 765, Size = 55.55, Distance_to_the_star = 666 });

            // ��� ������� ��������
            AddingState expected = AddingState.Fail;

            // ��� ��������
            AddingState actual = _del.DeleteObj(Name, list);

            // ����������
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Get_list_and_Type_Cosmic_objects_for_serialize_and_Compare_with_ReadingXmlFile()
        {
            // ������ �� ����
            string handle = "Serialize Planets";
            List<object> list = new List<object>();
            list.Add(new Planet() { PlanetID = 1, Name = "Versus", Radius = 345, Size = 34.54, Speed_Planet = 666 });
            list.Add(new Star() { StarID = 2, Name = "Sun", Radius = 765, Size = 55.55, Distance_to_the_star = 666 });
            //����������� � ����
            _xmlMethods.WriteToXml(list, handle);
            //��� ������� ��������
            List<object> expected = new List<object>();
            expected.Add(new Planet() { PlanetID = 1, Name = "Versus", Radius = 345, Size = 34.54, Speed_Planet = 666 });
            // ��� ��������
            List<object> actual = new List<object>();
            _xmlMethods.ReadToXml(ref actual);

            // ����������
            foreach (var item in actual)
            {
                Planet actual_planet = new Planet();
                actual_planet = item as Planet;
                foreach (var item2 in expected)
                {
                    Planet expected_planet = new Planet();
                    expected_planet = item2 as Planet;
                    Assert.AreEqual(expected_planet.Name, actual_planet.Name);
                    Assert.AreEqual(expected_planet.Radius, actual_planet.Radius);
                    Assert.AreEqual(expected_planet.Size, actual_planet.Size);
                    Assert.AreEqual(expected_planet.Speed_Planet, actual_planet.Speed_Planet);
                }

            }
        }
    }
}